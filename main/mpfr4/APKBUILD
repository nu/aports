# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=mpfr4
pkgver=4.2.0_p12
pkgrel=0
# strip p value, the patches are separate and are on https://www.mpfr.org/mpfr-current/
_pkgver="${pkgver%_*}"
pkgdesc="multiple-precision floating-point library"
url="https://www.mpfr.org/"
arch="all"
license="LGPL-3.0-or-later"
makedepends_build="texinfo"
makedepends_host="gmp-dev"
makedepends="$makedepends_build $makedepends_host"

# mpfr is special because we cannot break ABI without first rebuild gcc.
# so we need be able to have multiple version of mpfr installed in parallel
# we also need be able to calculate bulid order from global scope, otherwise
# it is not possible to calculate build order til after package is built.
# Therfore we call -dev package mpfr-dev instead of mpfr4-dev with a
# provides=mpfr-dev
subpackages="$pkgname-doc mpfr-dev"

source="https://www.mpfr.org/mpfr-$_pkgver/mpfr-$_pkgver.tar.xz
	p12.patch
	"
builddir="$srcdir/mpfr-$_pkgver"

provides="mpfr=$pkgver-r$pkgrel"
replaces="mpfr"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--with-sysroot="$CBUILDROOT" \
		--prefix=/usr \
		--enable-shared
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
58e843125884ca58837ae5159cd4092af09e8f21931a2efd19c15de057c9d1dc0753ae95c592e2ce59a727fbc491af776db8b00a055320413cdcf2033b90505c  mpfr-4.2.0.tar.xz
c3de1907648cbf1a87d292bc017f1f651ed4449eb28459187821199016e0f12e7cb3b45d6163c9a4361d3b7499f5b6f771b271edcfa93399f3c28c1180b3b387  p12.patch
"
