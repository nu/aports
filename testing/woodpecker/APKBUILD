# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: leso-kn <info@lesosoftware.com>
pkgname=woodpecker
pkgver=0.15.11
pkgrel=0
pkgdesc="Woodpecker is a community fork of the Drone CI system."
url="https://woodpecker-ci.org"
# s390x, riscv64: currently not supported by esbuild
arch="all !s390x !riscv64"
license="Apache-2.0"
pkgusers="woodpecker"
pkggroups="woodpecker"
makedepends="go sqlite-dev yarn"
subpackages="$pkgname-doc $pkgname-openrc"
install="$pkgname.pre-install"
source="https://github.com/woodpecker-ci/woodpecker/releases/download/v$pkgver/woodpecker-src-$pkgver.tar.gz
	no-ext-static.patch
	woodpecker.initd
	woodpecker.confd
	woodpecker.conf
	"
builddir="$srcdir"

export GOFLAGS="$GOFLAGS -tags=libsqlite3"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	export TARGETARCH="$(go env GOHOSTARCH)"
	make
}

check() {
	go test ./...
}

package() {
	install -Dm755 "$srcdir"/dist/woodpecker-server \
		"$pkgdir"/usr/bin/woodpecker-server
	install -Dm755 "$srcdir"/dist/woodpecker-agent \
		"$pkgdir"/usr/bin/woodpecker-agent
	install -Dm755 "$srcdir"/dist/woodpecker-cli \
		"$pkgdir"/usr/bin/woodpecker-cli
	install -Dm 644 "$srcdir"/LICENSE \
		"$pkgdir"/usr/share/licenses/$pkgname/LICENSE

	install -Dm755 "$srcdir"/woodpecker.initd \
		"$pkgdir"/etc/init.d/woodpecker
	install -Dm644 "$srcdir"/woodpecker.confd \
		"$pkgdir"/etc/conf.d/woodpecker

	install -Dm640 -o woodpecker -g woodpecker "$srcdir"/woodpecker.conf \
		"$pkgdir"/etc/woodpecker.conf
	install -dm755 -o woodpecker -g woodpecker "$pkgdir"/var/lib/woodpecker
}

sha512sums="
e3eeb79c9d9c1d91978d8024bc642bd62f574a43e44be9a01b1623fa8a40b004691bb74040eadb63b6c0a1169ad4e2db92d612de293a91c6df0cc4155ea9f233  woodpecker-src-0.15.11.tar.gz
cf392ef7db2cf3cdd7502f216718f087671aa8e67f335250947f0499b681ab1ea9bd0477fdeb7251db13fd0613030d876d54d77ec85a0df38c13a6a47b10e850  no-ext-static.patch
69fe477f805dcb71b0220b9af2b3d0226b2e92f3c46764f37a139bb7303ad7cdb1caa2a711d1f9d22fccb357bbfbecb1c6cba2033c9101a11c0bb67b405c3e55  woodpecker.initd
0be91432e730cb0ad3663bebe7a257437cbefe5fa5c2f3145d621545d6cd2ff89ae41f338a5874166d2b03dc8caab73d26cd4322ed1122d4949cae5d6002b823  woodpecker.confd
cb15d7ff290d9b68d5f63c20401ab622c8a7067d336841c876a6d3325e9d2a3ede3a85b792131d7d77a4126cbdb6f30a5a6113468f1efd736a2c1bbf2bfefbe4  woodpecker.conf
"
