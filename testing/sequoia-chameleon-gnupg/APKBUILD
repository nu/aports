# Maintainer: psykose <alice@ayaya.dev>
pkgname=sequoia-chameleon-gnupg
pkgver=0.3.0
pkgrel=0
pkgdesc="Sequoia's reimplementation of the GnuPG interface"
url="https://sequoia-pgp.org/"
# ring
arch="all !ppc64le !s390x !riscv64"
license="GPL-3.0-or-later"
makedepends="
	bzip2-dev
	cargo
	cargo-auditable
	clang15-dev
	nettle-dev
	openssl-dev
	sqlite-dev
	"
source="https://gitlab.com/sequoia-pgp/sequoia-chameleon-gnupg/-/archive/v$pkgver/sequoia-chameleon-gnupg-v$pkgver.tar.gz
	getrandom.patch
	"
builddir="$srcdir/$pkgname-v$pkgver"
options="net !check" # bunch of failures against gpg cli

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/gpgv-sq target/release/gpg-sq \
		-t "$pkgdir"/usr/bin
}

sha512sums="
ad0d9297727ec9cc4c381af511cfb61cb9ee7f7cc897a7d1d1d36bc27bc82fe79837ceb8e5efce8f7cda1ff5ee286b30d5cf503fd5878c9f781c4a407ca8ba13  sequoia-chameleon-gnupg-v0.3.0.tar.gz
38c97cd068c294db3ed1470578fa5b177c0baeb52a88cf1302149504501653c2808110f959c623297fdf86c22370fa7f547a487c2020e4766f1fe036833bad76  getrandom.patch
"
