# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=sport-activities-features-gui
pkgver=0.2.1
pkgrel=0
pkgdesc="GUI for sport-activities-features package"
url="https://github.com/firefly-cpp/sport-activities-features-gui"
arch="noarch !s390x !riscv64 !ppc64le" # py3-sport-activities-features
license="MIT"
depends="py3-sport-activities-features py3-qtawesome py3-qt6"
makedepends="py3-poetry-core py3-gpep517"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/firefly-cpp/sport-activities-features-gui/archive/$pkgver/sport-activities-features-gui-$pkgver.tar.gz"
options="!check" # upstream provides no tests for now (only version check)

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl

	install -Dm644 AppData/io.github.firefly-cpp.sport_activities_features_gui.desktop "$pkgdir"/usr/share/applications/sport_activities_features_gui.desktop
	install -Dm644 AppData/icon1.png "$pkgdir"/usr/share/icons/hicolor/256x256/apps/sport-activities-features-gui.png
}

sha512sums="
506a4368a07a5389f6ef43b5bb1f9fff6b6f408116930d6c5ced93689a2f8260517691c84e3652858746d8bf8463a075ce7ef5ef3804600b72131af9e441e892  sport-activities-features-gui-0.2.1.tar.gz
"
