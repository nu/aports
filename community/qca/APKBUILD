# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qca
pkgver=2.3.6
pkgrel=1
pkgdesc="Qt cryptographic architecture"
url="https://userbase.kde.org/QCA"
arch="all"
license="LGPL-2.1-or-later"
depends_dev="
	cyrus-sasl-dev
	qt5-qtbase-dev
	qt6-qt5compat-dev
	qt6-qtbase-dev
	"
makedepends="$depends_dev
	cmake
	doxygen
	graphviz
	openssl-dev>3
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc lib$pkgname-qt5:libs_qt5 lib$pkgname-qt6:libs_qt6"
source="https://download.kde.org/stable/qca/$pkgver/qca-$pkgver.tar.xz"

build() {
	cmake -B build-qt5 -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DWITH_cyrus-sasl_PLUGIN=yes \
		-DQT6=OFF
	cmake -B build-qt6 -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DWITH_cyrus-sasl_PLUGIN=yes \
		-DQT6=ON

	cmake --build build-qt5
	cmake --build build-qt6
}

check() {
	ctest --test-dir build-qt5 --output-on-failure
	ctest --test-dir build-qt6 --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build-qt5
	DESTDIR="$pkgdir" cmake --install build-qt6
}

libs_qt5() {
	amove usr/lib/qca-qt5
	amove usr/lib/libqca*qt5*
}

libs_qt6() {
	amove usr/lib/qca-qt6
	amove usr/lib/libqca*qt6*
}

sha512sums="
018bde919df28cfc9e5d6c5ad30724199a1a17437022751fb92bfc1ce691d8a56c62b661526e346f5a0c5ff7ffd556499f2ee25efe9f8b1698b3f8eee480811c  qca-2.3.6.tar.xz
"
