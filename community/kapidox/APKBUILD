# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kapidox
pkgver=5.108.0
pkgrel=1
arch="noarch !armhf" # armhf blocked by extra-cmake-modules
pkgdesc="Scripts and data for building API documentation (dox) in a standard format and style"
url="https://community.kde.org/Frameworks"
license="BSD-3-Clause"
depends="
	doxygen
	py3-jinja2
	py3-yaml
	python3
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	python3-dev
	samurai
	"
checkdepends="bash"
subpackages="$pkgname-pyc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kapidox-$pkgver.tar.xz"
options="!check" # No useful tests

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kapidox.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
fe8c420b250c980ee6b09dae453211f0e617073f2a2668b70dcb3fb04cae20236d45012fe827a9829c8415fd4f5d295dea52e527159f4998afb2d435d29ca2ef  kapidox-5.108.0.tar.xz
"
