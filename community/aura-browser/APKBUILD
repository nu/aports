# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=aura-browser
pkgver=5.27.6
pkgrel=0
pkgdesc="Browser for a fully immersed Big Screen experience allowing you to navigate the world wide web using just your remote control"
url="https://invent.kde.org/plasma-bigscreen/aura-browser"
# armhf blocked by extra-cmake-modules
# s390x, ppc64le and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !s390x !ppc64le !riscv64"
license="GPL-2.0-or-later"
depends="
	kirigami2
	qt5-qtvirtualkeyboard
	"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtmultimedia-dev
	qt5-qtquickcontrols2-dev
	qt5-qtwebengine-dev
	samurai
	"
case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/aura-browser-$pkgver.tar.xz"
options="!check" # No tests

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/aura-browser.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
16e3ba3a614afeb8fd895e161513b84fef59a8297cfa59b8c31651e6e3682ade0917d1ce3035cb4dbcc5167168c1539e8c8679e1bfeaf5d1c80678b0e2920ffe  aura-browser-5.27.6.tar.xz
"
