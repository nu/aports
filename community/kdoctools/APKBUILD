# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kdoctools
pkgver=5.108.0
pkgrel=1
pkgdesc="Documentation generation from docbook"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends="
	docbook-xml
	docbook-xsl
	"
depends_dev="
	karchive-dev
	ki18n-dev
	libxml2-dev
	libxml2-utils
	libxslt-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	perl-uri
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdoctools-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kdoctools.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
307bb472fa8bbb4165bce174fccb321498fde81e40aaa0f540c7cb9091674ae4f43210f9ad405e063a95ff3c0b518e0c0d609a03f82b3c3101ea29b6f47ed910  kdoctools-5.108.0.tar.xz
"
