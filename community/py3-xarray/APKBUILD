# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-xarray
_pkgorig=xarray
pkgver=2023.7.0
pkgrel=0
pkgdesc="N-D labeled arrays and datasets in Python"
url="https://xarray.dev"
arch="noarch !s390x" # assertionErrors
license="Apache-2.0"
depends="python3 py3-numpy py3-packaging py3-pandas"
makedepends="python3-dev py3-setuptools_scm"
checkdepends="py3-coverage py3-mock py3-pytest py3-pytest-cov"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/x/xarray/xarray-$pkgver.tar.gz"
builddir="$srcdir/$_pkgorig-$pkgver"

build() {
	python3 setup.py build
}

check() {
	python3 -m pytest -k 'not test_dataset and not test_distributed and not test_dataarray and not test_accessor_str'
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	rm -r "$pkgdir"/usr/lib/python3.*/site-packages/xarray/tests
}

sha512sums="
88863770b22978ba681a2d970453ae087a234fcfb68f047b6020b39841a91207e8d92e01486d58bbeadac8d14608aa0c744ee9b6d54890f52fe3537b28292239  py3-xarray-2023.7.0.tar.gz
"
