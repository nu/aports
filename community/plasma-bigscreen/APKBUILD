# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-bigscreen
pkgver=5.27.6
pkgrel=0
pkgdesc="A 10-feet interface made for TVs"
url="https://invent.kde.org/plasma/plasma-bigscreen/"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
license="LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL"
depends="
	kdeconnect
	kirigami2
	plasma-nano
	plasma-nm
	plasma-pa
	plasma-remotecontrollers
	plasma-settings
	plasma-workspace
	"
makedepends="
	extra-cmake-modules
	kactivities-dev
	kactivities-stats-dev
	kcmutils-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	kirigami2-dev
	knotifications-dev
	kwayland-dev
	kwindowsystem-dev
	plasma-framework-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtmultimedia-dev
	samurai
	"
subpackages="$pkgname-lang"
case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-bigscreen-$pkgver.tar.xz"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/plasma-bigscreen.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
97a64d1d64dd3f2fca94fcd58f004804455931bf2010f27a9eb43345d0988ceba90d6f34cf46cdbfe0bfbb433b279c87243c64bc95d7f56e02d027c59826d903  plasma-bigscreen-5.27.6.tar.xz
"
