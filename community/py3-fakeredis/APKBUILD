# Maintainer: Leonardo Arena <rnalrd@alpinelinux.org>
pkgname=py3-fakeredis
_pkgname=${pkgname#py3-}
pkgver=2.17.0
pkgrel=0
pkgdesc="Fake implementation of redis API for testing purposes"
url="https://pypi.org/project/fakeredis/"
arch="noarch"
license="BSD-3-Clause"
depends="py3-packaging py3-redis py3-sortedcontainers"
makedepends="py3-poetry-core py3-gpep517"
checkdepends="py3-hypothesis py3-pytest py3-pytest-mock py3-lupa"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir"/$_pkgname-$pkgver
options="!check" # no tests in tarball

prepare() {
	default_prepare
	# Requires unpackaged 'aioredis'
	rm -f test/test_aioredis*.py
}

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
31da92e0617a6999bb29261b9efdb1b8ac9f790eb1f4500f71d65b7e2ec700095ac30a55230f3c3efe9cd9c535d04a9138596944e2aa684a1e6e9e7158b5c2be  py3-fakeredis-2.17.0.tar.gz
"
