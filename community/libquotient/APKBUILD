# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libquotient
pkgver=0.8.0
pkgrel=1
pkgdesc="Qt library for cross-platform clients for Matrix"
url="https://github.com/quotient-im/libQuotient"
arch="all !armhf" # blocked by qt5-qtmultimedia
license="LGPL-2.1-or-later"
depends_dev="
	olm-dev
	qt5-qtbase-dev
	qtkeychain-dev
	qt5-qtmultimedia-dev
	"
makedepends="$depends_dev
	cmake
	samurai
	"
subpackages="$pkgname-qt5 $pkgname-dev"
source="https://github.com/quotient-im/libQuotient/archive/$pkgver/libQuotient-$pkgver.tar.gz"
builddir="$srcdir/libQuotient-$pkgver"

build() {
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DBUILD_TESTING=True \
		-DQuotient_ENABLE_E2EE=True \
		-DQuotient_INSTALL_TESTS=False
	cmake --build build
}

check() {
	# testolmaccount requires a running Matrix homeserver
	ctest --test-dir build --output-on-failure -E "testolmaccount"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	# android only
	rm -r "$pkgdir"/usr/share/ndk-modules
}

qt5() {
	pkgdesc="$pkgdesc (qt5)"
	amove usr/lib/libQuotient.so.*
}

sha512sums="
5ea12d8c1e0091a0dd1a6130b9ca540f993ad560acbe6391522eacae6c38ef83665338bb407d0898d03bac5e0660b46db2f735b9ded35ff4b195a8d34c8c96ff  libQuotient-0.8.0.tar.gz
"
