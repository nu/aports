# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Lauren N. Liberda <lauren@selfisekai.rocks>
pkgname=element-web
pkgver=1.11.36
pkgrel=0
pkgdesc="A glossy Matrix collaboration client for the web"
url="https://element.io/"
arch="noarch"
options="!check"
license="Apache-2.0"
source="https://github.com/vector-im/element-web/releases/download/v$pkgver/element-v$pkgver.tar.gz"
builddir="$srcdir/element-v$pkgver"
install="$pkgname.post-upgrade"
provides="riot-web=$pkgver-r$pkgrel"
replaces="riot-web"

# secfixes:
#   1.11.30-r0:
#     - CVE-2023-30609
#   1.11.26-r0:
#     - CVE-2023-28103
#     - CVE-2023-28427
#   1.11.7-r0:
#     - CVE-2022-39249
#     - CVE-2022-39250
#     - CVE-2022-39251
#     - CVE-2022-39236
#   1.11.4-r0:
#     - CVE-2022-36059
#     - CVE-2022-36060
#   1.9.7-r0:
#     - CVE-2021-44538
#   1.8.4-r0:
#     - CVE-2021-40823
#     - CVE-2021-40824

build() {
	return 0
}

package() {
	mkdir -p "$pkgdir"/usr/share/webapps \
		"$pkgdir"/etc/element-web
	cp -r "$builddir" "$pkgdir"/usr/share/webapps/element-web
	ln -s ../element-web "$pkgdir"/usr/share/webapps/riot-web
	mv "$pkgdir"/usr/share/webapps/element-web/config.sample.json \
		"$pkgdir"/etc/element-web
	ln -sf /etc/element-web/config.json \
		"$pkgdir"/usr/share/webapps/element-web/config.json
}

sha512sums="
03aa1530360d479c2970e2fdee8b23d526701346b3b26f3052c72226644ae61ecb1f6df58efccea4d30db8ffa794c6c58893eb7938bde24f5fd42d319618efa5  element-v1.11.36.tar.gz
"
