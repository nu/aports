# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kaccounts-integration
pkgver=23.04.3
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x, ppc64le and riscv64 blocked by signon-ui -> qt5-qtwebengine
arch="all !armhf !s390x !ppc64le !riscv64"
url="https://kde.org/applications/internet/"
pkgdesc="Small system to administer web accounts for the sites and services across the KDE desktop"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="
	accounts-qml-module
	signon-ui
	"
depends_dev="
	kcmutils-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	ki18n-dev
	libaccounts-qt-dev
	qcoro-dev
	qt5-qtbase-dev
	signond-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kaccounts-integration-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # No tests available

_commit=""
_repo_url="https://invent.kde.org/network/kaccounts-integration.git"
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 $_repo_url .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f06db87e3b67e2c0cccdaa3a415f7e1b06d9798bdfe54a449c17293a8438e7142fc6be142023f4fe8417fe89294a2bca80b98834c0aae10f02b0d8f1594fc511  kaccounts-integration-23.04.3.tar.xz
"
