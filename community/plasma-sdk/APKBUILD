# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-sdk
pkgver=5.27.6
pkgrel=0
pkgdesc="Applications useful for Plasma Development"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="
	kirigami2
	qt5-qtquickcontrols
	"
makedepends="
	extra-cmake-modules
	karchive-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kparts-dev
	kservice-dev
	ktexteditor-dev
	kwidgetsaddons-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtsvg-dev
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-sdk-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang $pkgname-zsh-completion"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/plasma-sdk.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

prepare() {
	default_prepare

	# This currently breaks CMake
	rm -r po/nl/docs/plasma-sdk
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# iconmodeltest is broken
	xvfb-run ctest --test-dir build --output-on-failure -E "iconmodeltest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
74f82eb09f362c445ddc1d46ddd5e02a848245ee0f84b332273a4376403c5273da66febf26583f32a1fe8e7223860a4e8ab0782f1d396139991da2941500630b  plasma-sdk-5.27.6.tar.xz
"
